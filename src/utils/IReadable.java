package utils;

public interface IReadable {
	public String read() throws Exception;
}
