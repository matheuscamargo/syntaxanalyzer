package parser;

public class NonTerminalSymbol extends Symbol {		
	@Override
	public boolean isTerminal() {
		return false;
	}
	
	public NonTerminalSymbol(String symbol) {
		super(symbol);
	}
}
