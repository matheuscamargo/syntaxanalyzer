package parser;

import java.util.Iterator;
import java.util.LinkedList;

/*
 * Class to encapsulate a sequence of
 * symbols.
 */
public class SymbolList implements Iterable<Symbol> {
	
	public SymbolList() {}
	
	public SymbolList(Symbol symbol) {
		this._symbols.add(symbol);
	}
	
	public SymbolList(SymbolList symbolList) {
		for (Symbol s: symbolList) {
			 _symbols.add(s);
		}
	}
	
	public void append (Symbol symbol) {
		_symbols.add(symbol);
	}
	
	public boolean equalsWithString(String str) {
		return str.equals(this.toString());
	}
	
	public NonTerminalSymbol firstNonTerminalSymbol() {
		for (Symbol s : _symbols) {
			if (!s.isTerminal())
				return (NonTerminalSymbol) s;
		}
		return null;
	}
	/**
	 * @return True if the string has only terminal symbols
	 */
	public boolean hasNonTerminalSymbol() {
//		System.out.println("prefix "+ prefix().length());
//		System.out.println("symbols "+ _symbols.size());
		return prefix().length() != _symbols.size();
	}
	
	/**
	 * @return The string until the first non-terminating Symbol
	 */
	public String prefix() {
		StringBuilder sb = new StringBuilder();
		for (Symbol s: _symbols) {
			if (!s.isTerminal())
				break;
			
			sb.append(s.getSymbol());
		}
		return sb.toString();
	}
	
	/**
	 * 
	 * @param subToSubstitute  Symbol to be replaced (and removed) in the list
	 * @param syms List of Symbols to replace
	 */
	public void substituteNonTerminalSymbol(NonTerminalSymbol subToSubstitute,
										    SymbolList syms) {
		if (!_symbols.contains(subToSubstitute))
			return;
		
		int substituteIndex = _symbols.indexOf(subToSubstitute);
		_symbols.remove(substituteIndex);
		
		for (Symbol s: syms) {
			_symbols.add(substituteIndex++, s);
		}
	}
	
	/**
	 * @return String associated with the list of symbols
	 */
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (Symbol s: _symbols) {
			sb.append(s.getSymbol());
		}		
		return sb.toString();
	}
	
	public Iterator<Symbol> iterator() {
		return _symbols.iterator();
	}
	
	private LinkedList<Symbol> _symbols = new LinkedList<>();
}
